from matplotlib import pyplot as plt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QLabel


class Picture:
    def __init__(self, np_arr):
        qimage = QImage(np_arr, np_arr.shape[0], np_arr.shape[1], QImage.Format_Grayscale8)
        self.pixmap = QPixmap(qimage)
        self.p_label = QLabel()
        self.p_label.setPixmap(self.pixmap)

    def update(self, new_arr):
        qimage = QImage(new_arr, new_arr.shape[0], new_arr.shape[1], QImage.Format_Grayscale8)
        self.pixmap = QPixmap(qimage)
        self.p_label.setPixmap(self.pixmap)
