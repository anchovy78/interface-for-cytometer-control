from Graph import Graph, np
from Picture import Picture
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, QLabel, QGridLayout, QSizePolicy, QSpacerItem, QFrame
from PyQt5.QtCore import Qt, QMetaObject
from PyQt5.QtGui import QFont


class UiForm(object):
    def __init__(self, form, w):
        form.setObjectName("form")
        font = QFont()
        font.setPixelSize(20)

        label_g1 = QLabel("First graph name")
        label_g2 = QLabel("Second graph name")
        label_p1 = QLabel("First cam name")
        label_p2 = QLabel("Second cam name")
        for lab in [label_g1, label_g2, label_p1, label_p2]:
            lab.setFont(font)
            lab.setAlignment(Qt.AlignHCenter)
            lab.setFrameStyle(QFrame.Box | QFrame.Raised)
            lab.setLineWidth(2)
            lab.setMidLineWidth(1)

        self.main_layout = QHBoxLayout(w)
        self.graph_layout = QVBoxLayout()
        self.g1 = Graph()
        self.g2 = Graph()
        self.graph_layout.addWidget(label_g1)
        self.graph_layout.addWidget(self.g1.canvas)
        self.graph_layout.addItem(QSpacerItem(600, 50, QSizePolicy.Minimum, QSizePolicy.Minimum))
        self.graph_layout.addWidget(label_g2)
        self.graph_layout.addWidget(self.g2.canvas)
        # self.graph_layout.setGeometry()
        self.main_layout.addLayout(self.graph_layout)

        self.cam_layout = QVBoxLayout()
        self.p1 = Picture(np.random.rand(640, 320))
        self.p2 = Picture(np.random.rand(640, 320))
        self.cam_layout.addWidget(label_p1)
        self.cam_layout.addWidget(self.p1.p_label)
        self.cam_layout.addItem(QSpacerItem(0, 50, QSizePolicy.Minimum, QSizePolicy.Minimum))
        self.cam_layout.addWidget(label_p2)
        self.cam_layout.addWidget(self.p2.p_label)
        self.main_layout.addLayout(self.cam_layout)
        
        self.third_layout = QVBoxLayout()
        l1 = QLabel("1 velocity")
        l1.setFont(font)
        self.v1 = QLabel()
        self.v1.setFont(font)
        self.v1.setNum(50)
        lay1 = QHBoxLayout()
        lay1.addWidget(l1)
        lay1.addWidget(self.v1)
        l2 = QLabel("2 velocity")
        l2.setFont(font)
        self.v2 = QLabel()
        self.v2.setFont(font)
        self.v2.setNum(50)
        lay2 = QHBoxLayout()
        lay2.addWidget(l2)
        lay2.addWidget(self.v2)
        self.ch1 = QPushButton("1")
        self.ch2 = QPushButton("2")
        self.ch3 = QPushButton("3")
        self.ch4 = QPushButton("4")
        self.third_layout.addLayout(lay1)
        self.third_layout.addLayout(lay2)
        self.third_layout.addItem(QSpacerItem(0, 50, QSizePolicy.Minimum, QSizePolicy.Minimum))
        for b in [self.ch1, self.ch2, self.ch3, self.ch4]:
            self.third_layout.addWidget(b)
        self.third_layout.addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding))
        self.main_layout.addLayout(self.third_layout)

        QMetaObject.connectSlotsByName(form)
