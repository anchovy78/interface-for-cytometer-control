from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np


class Graph:

    def __init__(self):
        self.canvas = FigureCanvas()
        self.x = np.arange(1)
        self.y = np.arange(1)
        self.plot = self.canvas.figure.add_subplot(111)
        self.plot.plot(self.x, self.y, color='r')

    def update(self, new_x_y):
        self.plot.clear()
        self.x = new_x_y[0].copy()
        self.y = new_x_y[1].copy()
        self.plot.set_xlabel("X ax")
        self.plot.set_ylabel("Y ax")
        self.plot.plot(self.x, self.y, color='r')
        self.canvas.draw()
