import time
import UI
from Graph import Graph, np
import sys
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QDesktopWidget
from PyQt5 import QtCore


class CamHandler(QtCore.QObject):
    h_signal = QtCore.pyqtSignal(np.ndarray)

    def run(self):
        while True:
            self.h_signal.emit(np.zeros((640, 320)))
            QtCore.QThread.msleep(500)

            self.h_signal.emit(np.ones((640, 320)))
            QtCore.QThread.msleep(500)


class GraphHandler(QtCore.QObject):
    h_signal = QtCore.pyqtSignal(np.ndarray)

    def run(self):
        while True:
            self.h_signal.emit(np.array([np.arange(100), np.random.random(100)]))
            QtCore.QThread.msleep(1000)


class ApplicationWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self._main = QWidget()
        self.setCentralWidget(self._main)
        self.ui = UI.UiForm(self, self._main)
        self.move(100, 100)
        self.g1_thread = QtCore.QThread()
        self.g2_thread = QtCore.QThread()
        self.p1_thread = QtCore.QThread()
        self.p2_thread = QtCore.QThread()
        self.g1_handler = GraphHandler()
        self.g2_handler = GraphHandler()
        self.p1_handler = CamHandler()
        self.p2_handler = CamHandler()
        self.g1_handler.moveToThread(self.g1_thread)
        self.g2_handler.moveToThread(self.g2_thread)
        self.p1_handler.moveToThread(self.p1_thread)
        self.p2_handler.moveToThread(self.p2_thread)
        self.g1_handler.h_signal.connect(self.ui.g1.update)
        self.g2_handler.h_signal.connect(self.ui.g2.update)
        self.p1_handler.h_signal.connect(self.ui.p1.update)
        self.p2_handler.h_signal.connect(self.ui.p2.update)
        self.g1_thread.started.connect(self.g1_handler.run)
        self.g2_thread.started.connect(self.g2_handler.run)
        self.p1_thread.started.connect(self.p1_handler.run)
        self.p2_thread.started.connect(self.p2_handler.run)
        for x in [self.g1_thread, self.g2_thread, self.p1_thread, self.p2_thread]:
            x.start()
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = ApplicationWindow()
    sys.exit(app.exec_())

# self._main = QWidget()
# self.setCentralWidget(self._main)
# layout = QVBoxLayout(self._main)
# layout1 = QHBoxLayout()
# layout11 = QVBoxLayout()
# layout12 = QVBoxLayout()
# self.g1 = Graph()
# self.g2 = Graph()
# layout11.addWidget(self.g1.canvas)
# layout12.addWidget(self.g2.canvas)
# self.addToolBar(NavigationToolbar(self.g1.canvas, self))
# self.addToolBar(NavigationToolbar(self.g2.canvas, self))
# self.Update1 = QPushButton('Update1')
# self.Update2 = QPushButton('Update2')
# layout11.addWidget(self.Update1)
# layout12.addWidget(self.Update2)
# self.Update1.clicked.connect(self._up1)
# self.Update2.clicked.connect(self._up2)
# layout1.addLayout(layout11)
# layout1.addLayout(layout12)
# layout2 = QHBoxLayout()
# layout21 = QVBoxLayout()
# layout22 = QVBoxLayout()
# self.p1 = Picture(np.random.rand(640, 320))
# self.p2 = Picture(np.random.rand(640, 320))
# layout21.addWidget(self.p1.p_label)
# layout22.addWidget(self.p2.p_label)
# self.Update3 = QPushButton('Update3')
# self.Update4 = QPushButton('Update4')
# layout21.addWidget(self.Update3)
# layout22.addWidget(self.Update4)
# self.Update3.clicked.connect(self._up3)
# self.Update4.clicked.connect(self._up4)
# layout2.addLayout(layout21)
# layout2.addLayout(layout22)
# layout.addLayout(layout1)
# layout.addLayout(layout2)